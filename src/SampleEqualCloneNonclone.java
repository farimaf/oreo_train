import java.io.*;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Farima on 7/12/2017.
 */
public class SampleEqualCloneNonclone {

    public static void main(String[] args) {
        SampleEqualCloneNonclone sampler = new SampleEqualCloneNonclone();
//        sampler.sampleOneNode();
        //sampler.sampleOneNodeByCount(2127306);//number of either clones or nonclones is given
        //sampler.sampleFromMultipleFilesByCountFromZip(2178531);
        //sampler.sampleFromMultipleFilesByCount(2178531);
        sampler.sampleFromMultipleFilesNoCount();
        //sampler.sampleFromMultipleFilesFromZipNoCount();
    }

    private String pathFirstPart = "/scratch/mondego/local/farima/new_oreo/train_related/mlcc/SourcererCC/clone-detector/NODE_*";
    //    private String pathSecondPart= "/output7.0/queryclones_index_WITH_FILTER_2.txt";
    private String pathSecondPart = "/output7.0/train_3_*.csv";
    private String pathSingleNode = "/scratch/mondego/local/farima/new_oreo/train_related/mlcc/SourcererCC/clone-detector/NODE_1/output6.0/type_3.1.csv";
//    private String pathSingleNode = "sampletest.txt";
    private int nodesCount = 9;
    //private String inputPath= "./output/train_integrated/train_doublefeature.txt";
    private HashMap<String, HashSet<Integer>> cloneLinesMultipleNodes = new HashMap<>();//for multiple nodes
    private HashSet<Integer> cloneLinesSingleNode = new HashSet<>();
    private HashSet<String> randomNoncloneLinesMultipleNodes = new HashSet<>();
    private HashSet<Integer> randomNoncloneLinesSingleNode=new HashSet<>();
    //private final int SAMPLE_COUNT=22151199*2;
    //private final int SAMPLE_COUNT=1205138;
    private final int SAMPLE_COUNT = 3642279;
    private PrintWriter printWriter;
    private HashMap<String, Integer> lineNumbers = new HashMap<>();//for multiple nodes
    private int lineCount = 0;//for single node

    SampleEqualCloneNonclone() {
        try {
//            printWriter = new PrintWriter(Paths.get("/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results" +
//                    "/samples_shardbased/Node5_6_equalCloneNonCloneSample_shardbased.txt").toString());
           printWriter=new PrintWriter("D:\\equal.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sampleFromMultipleFilesByCountFromZip(int count){
        try {
            String zipDir = "2";
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
            String pathToZip = mainPath + zipDir + ".zip";
            //String pathToZip="D:\\farima.zip";
            HashMap<Integer, String> idsToFileNames = new HashMap<>();
            HashMap<String, Integer> fileNamesToLengths = new HashMap<>();
            HashMap<String, HashSet<Integer>> cloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> noncloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> randomFileLineNums = new HashMap<>();
            String line = "";
            int fileNum = 0;
            ZipFile zip = new ZipFile(pathToZip);
            for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir + "/candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        idsToFileNames.put(fileNum, entry.getName());
                        cloneInFiles.put(entry.getName(), new HashSet<>());
                        noncloneInFiles.put(entry.getName(), new HashSet<>());
                        fileNum++;
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            try {
                                if (line.split("~~")[2].equals("1")) {
                                    cloneInFiles.get(entry.getName()).add(lineNum);
                                } else if (line.split("~~")[2].equals("0")) {
                                    noncloneInFiles.get(entry.getName()).add(lineNum);
                                }
                                lineNum++;
                            }
                            catch (ArrayIndexOutOfBoundsException ex){
                                System.out.println("error in line: "+line);
                                ex.printStackTrace();
                                continue;
                            }
                        }
                        fileNamesToLengths.put(entry.getName(), lineNum);
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println(cloneInFiles.size());
            System.out.println(noncloneInFiles.size());
            Random random = new Random(0);
            int randFile = 0;
            int randLineInFile = 0;
            int clonesAdded = 0;
            int nonclonesAdded = 0;
            while (clonesAdded < count || nonclonesAdded < count) {
                randFile = random.nextInt(idsToFileNames.size());
                String fileName = idsToFileNames.get(randFile);
                randLineInFile = random.nextInt(fileNamesToLengths.get(fileName));
                if (!randomFileLineNums.containsKey(fileName)) {
                    randomFileLineNums.put(fileName, new HashSet<>());
                }

                if (!randomFileLineNums.get(fileName).contains(randLineInFile)) {
                    if (cloneInFiles.get(fileName).contains(randLineInFile) && clonesAdded < count) {
                        clonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    } else if (noncloneInFiles.get(fileName).contains(randLineInFile) && nonclonesAdded < count) {
                        nonclonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    }
                }
            }
            System.out.println("random making complete");
            zip = new ZipFile(pathToZip);
            int outputFilesize = 0;
            for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir + "/candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        HashSet<Integer> randomLineNums=randomFileLineNums.get(entry.getName());
                        line = "";
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            if (randomLineNums.contains(lineNum)) {
                                outputFilesize++;
                                writeToFile(line);
                                System.out.println(line);
                            }
                            lineNum++;
                        }
                    }
                }
            }

            System.out.println("Write Complete. number of lines in file: " + outputFilesize);

            printWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sampleFromMultipleFilesByCount(int count){
        try {
            String dir = "3";
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
            String pathToDir = mainPath + dir ;
           // String pathToDir="D:\\farima";
            HashMap<Integer, String> idsToFileNames = new HashMap<>();
            HashMap<String, Integer> fileNamesToLengths = new HashMap<>();
            HashMap<String, HashSet<Integer>> cloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> noncloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> randomFileLineNums = new HashMap<>();
            String line = "";
            int fileNum = 0;
            File folder = new File(pathToDir);
            for (final File entry : folder.listFiles()) {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(entry));
                        idsToFileNames.put(fileNum, entry.getName());
                        cloneInFiles.put(entry.getName(), new HashSet<>());
                        noncloneInFiles.put(entry.getName(), new HashSet<>());
                        fileNum++;
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            try {
                                if (line.split("~~")[2].equals("1")) {
                                    cloneInFiles.get(entry.getName()).add(lineNum);
                                } else if (line.split("~~")[2].equals("0")) {
                                    noncloneInFiles.get(entry.getName()).add(lineNum);
                                }
                                lineNum++;
                            }
                            catch (ArrayIndexOutOfBoundsException ex){
                                System.out.println("error in line: "+line);
                                ex.printStackTrace();
                                continue;
                            }
                        }
                        fileNamesToLengths.put(entry.getName(), lineNum);
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println(cloneInFiles.size());
            System.out.println(noncloneInFiles.size());
            Random random = new Random(0);
            int randFile = 0;
            int randLineInFile = 0;
            int clonesAdded = 0;
            int nonclonesAdded = 0;
            while (clonesAdded < count || nonclonesAdded < count) {
                randFile = random.nextInt(idsToFileNames.size());
                String fileName = idsToFileNames.get(randFile);
                randLineInFile = random.nextInt(fileNamesToLengths.get(fileName));
                if (!randomFileLineNums.containsKey(fileName)) {
                    randomFileLineNums.put(fileName, new HashSet<>());
                }

                if (!randomFileLineNums.get(fileName).contains(randLineInFile)) {
                    if (cloneInFiles.get(fileName).contains(randLineInFile) && clonesAdded < count) {
                        clonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    } else if (noncloneInFiles.get(fileName).contains(randLineInFile) && nonclonesAdded < count) {
                        nonclonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    }
                }
            }
            System.out.println("random making complete");
            int outputFilesize = 0;
            folder = new File(pathToDir);
            for (final File entry : folder.listFiles()) {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(entry));
                        HashSet<Integer> randomLineNums=randomFileLineNums.get(entry.getName());
                        line = "";
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            if (randomLineNums.contains(lineNum)) {
                                outputFilesize++;
                                writeToFile(line);
                                System.out.println(line);
                            }
                            lineNum++;
                        }
                    }
                }
            }

            System.out.println("Write Complete. number of lines in file: " + outputFilesize);

            printWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sampleFromMultipleFilesNoCount(){
        try {
            String dir = "5_6";
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
            String pathToDir = mainPath + dir ;
            //String pathToDir="D:\\farima";
            HashMap<Integer, String> idsToFileNames = new HashMap<>();
            HashMap<String, Integer> fileNamesToLengths = new HashMap<>();
            HashMap<String, HashSet<Integer>> cloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> noncloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> randomFileLineNums = new HashMap<>();
            String line = "";
            int fileNum = 0;
            int cloneCount=0;
            File folder = new File(pathToDir);
            for (final File entry : folder.listFiles()) {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(entry));
                        idsToFileNames.put(fileNum, entry.getName());
                        cloneInFiles.put(entry.getName(), new HashSet<>());
                        noncloneInFiles.put(entry.getName(), new HashSet<>());
                        fileNum++;
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            try {
                                if (line.split("~~")[2].equals("1")) {
                                    cloneInFiles.get(entry.getName()).add(lineNum);
                                    cloneCount++;
                                } else if (line.split("~~")[2].equals("0")) {
                                    noncloneInFiles.get(entry.getName()).add(lineNum);
                                }
                                lineNum++;
                            }
                            catch (ArrayIndexOutOfBoundsException ex){
                                System.out.println("error in line: "+line);
                                ex.printStackTrace();
                                continue;
                            }
                        }
                        fileNamesToLengths.put(entry.getName(), lineNum);
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println(cloneInFiles.size());
            System.out.println(noncloneInFiles.size());
            System.out.println(cloneCount);
            Random random = new Random(0);
            int randFile = 0;
            int randLineInFile = 0;
            int nonclonesAdded = 0;
            while (nonclonesAdded < cloneCount) {
                randFile = random.nextInt(idsToFileNames.size());
                String fileName = idsToFileNames.get(randFile);
                randLineInFile = random.nextInt(fileNamesToLengths.get(fileName));

                if (!randomFileLineNums.containsKey(fileName)) {
                    randomFileLineNums.put(fileName, new HashSet<>());
                }

                if (!randomFileLineNums.get(fileName).contains(randLineInFile)) {
                    if (noncloneInFiles.get(fileName).contains(randLineInFile) && nonclonesAdded < cloneCount) {
                        nonclonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    }
                }
            }
            System.out.println("random making complete");
            int outputFilesize = 0;
            folder = new File(pathToDir);
            for (final File entry : folder.listFiles()) {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new FileReader(entry));
                        HashSet<Integer> randomLineNums=randomFileLineNums.get(entry.getName());
                        HashSet<Integer> cloneLines=cloneInFiles.get(entry.getName());
                        line = "";
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            if (randomLineNums.contains(lineNum) || cloneLines.contains(lineNum)) {
                                outputFilesize++;
                                writeToFile(line);
                                System.out.println(line);
                            }
                            lineNum++;
                        }
                    }
                }
            }

            System.out.println("Write Complete. number of lines in file: " + outputFilesize);

            printWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sampleFromMultipleFilesFromZipNoCount(){
        try {
            String zipDir = "2";
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
            String pathToZip = mainPath + zipDir + ".zip";
            //String pathToZip="D:\\farima.zip";
            HashMap<Integer, String> idsToFileNames = new HashMap<>();
            HashMap<String, Integer> fileNamesToLengths = new HashMap<>();
            HashMap<String, HashSet<Integer>> cloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> noncloneInFiles = new HashMap<>();
            HashMap<String, HashSet<Integer>> randomFileLineNums = new HashMap<>();
            String line = "";
            int fileNum = 0;
            int cloneCount=0;
            ZipFile zip = new ZipFile(pathToZip);
            for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir + "/candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        idsToFileNames.put(fileNum, entry.getName());
                        cloneInFiles.put(entry.getName(), new HashSet<>());
                        noncloneInFiles.put(entry.getName(), new HashSet<>());
                        fileNum++;
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            try {
                                if (line.split("~~")[2].equals("1")) {
                                    cloneInFiles.get(entry.getName()).add(lineNum);
                                    cloneCount++;
                                } else if (line.split("~~")[2].equals("0")) {
                                    noncloneInFiles.get(entry.getName()).add(lineNum);
                                }
                                lineNum++;
                            }
                            catch (ArrayIndexOutOfBoundsException ex){
                                System.out.println("error in line: "+line);
                                ex.printStackTrace();
                                continue;
                            }
                        }
                        fileNamesToLengths.put(entry.getName(), lineNum);
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println(cloneInFiles.size());
            System.out.println(noncloneInFiles.size());
            System.out.println(cloneCount);
            Random random = new Random(0);
            int randFile = 0;
            int randLineInFile = 0;
            int nonclonesAdded = 0;
            while (nonclonesAdded < cloneCount) {
                randFile = random.nextInt(idsToFileNames.size());
                String fileName = idsToFileNames.get(randFile);
                randLineInFile = random.nextInt(fileNamesToLengths.get(fileName));

                if (!randomFileLineNums.containsKey(fileName)) {
                    randomFileLineNums.put(fileName, new HashSet<>());
                }

                if (!randomFileLineNums.get(fileName).contains(randLineInFile)) {
                    if (noncloneInFiles.get(fileName).contains(randLineInFile) && nonclonesAdded < cloneCount) {
                        nonclonesAdded++;
                        randomFileLineNums.get(fileName).add(randLineInFile);
                    }
                }
            }
            System.out.println("random making complete");
            int outputFilesize = 0;
            zip = new ZipFile(pathToZip);
            for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                ZipEntry entry = (ZipEntry) e.nextElement();
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir + "/candidatesList")) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        HashSet<Integer> randomLineNums=randomFileLineNums.get(entry.getName());
                        HashSet<Integer> cloneLines=cloneInFiles.get(entry.getName());
                        line = "";
                        int lineNum = 0;
                        while ((line = bufferedReader.readLine()) != null) {
                            if (randomLineNums.contains(lineNum) || cloneLines.contains(lineNum)) {
                                outputFilesize++;
                                writeToFile(line);
                                System.out.println(line);
                            }
                            lineNum++;
                        }
                    }
                }
            }

            System.out.println("Write Complete. number of lines in file: " + outputFilesize);

            printWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void sampleOneNodeByCount(int count){
        try {
            HashSet<Integer> cloneLines=new HashSet<>();
            HashSet<Integer> noncloneLines=new HashSet<>();
            HashSet<Integer> randomLineNums=new HashSet<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(pathSingleNode));
            String line = "";
            int lineNum = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.split("~~")[2].equals("1")) {
                    cloneLines.add(lineNum);
                }
                else if (line.split("~~")[2].equals("0")) {
                    noncloneLines.add(lineNum);
                }
                lineNum++;
            }
            lineCount=lineNum;
            Random random = new Random(0);
            int randNumLine = 0;
            int clonesAdded=0;
            int nonclonesAdded=0;
            while (randomLineNums.size() < count*2 ) {
                randNumLine = random.nextInt(lineCount);
                if (!randomLineNums.contains(randNumLine)) {
                    if (!cloneLines.contains(randNumLine) && clonesAdded<count) {
                        clonesAdded++;
                        randomLineNums.add(randNumLine);
                    }
                    else if (!noncloneLines.contains(randNumLine) && nonclonesAdded<count) {
                        nonclonesAdded++;
                        randomLineNums.add(randNumLine);
                    }
                }
            }
            System.out.println("random making complete");
            bufferedReader = new BufferedReader(new FileReader(pathSingleNode));
            line = "";
            lineNum = 0;
            int filesize = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (randomLineNums.contains(lineNum) ) {
                    filesize++;
                    writeToFile(line);
                    System.out.println(line);
                }
                lineNum++;
            }

            System.out.println("Write Complete. number of lines in file: " + filesize);

            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sampleOneNode() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(pathSingleNode));
            String line = "";
            int lineNum = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.split("~~")[2].equals("1")) {
                    cloneLinesSingleNode.add(lineNum);
                }
                lineNum++;
            }
            lineCount=lineNum;
            System.out.println("Getting clone lines complete");
            System.out.println("number of clones: "+cloneLinesSingleNode.size());
            Random random = new Random(12);
            int randNumLine = 0;

            while (randomNoncloneLinesSingleNode.size() < cloneLinesSingleNode.size()) {
                randNumLine = random.nextInt(lineCount);
                if (!cloneLinesSingleNode.contains(randNumLine))
                    randomNoncloneLinesSingleNode.add(randNumLine);
            }
            System.out.println("random making complete");
            bufferedReader = new BufferedReader(new FileReader(pathSingleNode));
            line = "";
            lineNum = 0;
            int filesize = 0;
            while ((line = bufferedReader.readLine()) != null) {
                if (cloneLinesSingleNode.contains(lineNum) || randomNoncloneLinesSingleNode.contains(lineNum)) {
                    filesize++;
                    writeToFile(line);
                    System.out.println(line);
                }
                lineNum++;
            }

            System.out.println("Write Complete. number of lines in file: " + filesize);

            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sampleMultipleNodes() {
        String[] fileChance = {"NODE_1", "NODE_1", "NODE_1", "NODE_1", "NODE_1", "NODE_1", "NODE_2", "NODE_2", "NODE_2", "NODE_3"};
        try {
            for (int i = 1; i <= nodesCount; i++) {
                String path = pathFirstPart + i + pathSecondPart;
                String nodeName = "NODE_" + i;
                BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
                String line = "";
                int lineNum = 0;
                cloneLinesMultipleNodes.put(nodeName, new HashSet<>());
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.split("~~")[2].equals("1")) {
                        cloneLinesMultipleNodes.get(nodeName).add(lineNum);
                    }
                    lineNum++;
                }
                System.out.println("1 read complete");
                lineNumbers.put(nodeName, lineNum);
            }

            Random random = new Random(12);
            int randNumNode = 0;
            int randNumLine = 0;

            while (randomNoncloneLinesMultipleNodes.size() < SAMPLE_COUNT) {
                randNumNode = random.nextInt(fileChance.length);
                String filePicked = fileChance[randNumNode];
                randNumLine = random.nextInt(lineNumbers.get(filePicked));
                if (!cloneLinesMultipleNodes.get(filePicked).contains(randNumLine))
                    randomNoncloneLinesMultipleNodes.add(filePicked + ":" + randNumLine);
            }
            System.out.println("random making complete");
            for (int i = 1; i <= nodesCount; i++) {
                String path = pathFirstPart + i + pathSecondPart;
                String nodeName = "NODE_" + i;
                BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
                String line = "";
                int lineNum = 0;
                int filesize = 0;
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.split("~~")[2].equals("1") || randomNoncloneLinesMultipleNodes.contains(nodeName + ":" + lineNum)) {
                        filesize++;
                        writeToFile(line);
                        System.out.println(line);
                    }
                    lineNum++;
                }

                System.out.println("one node written. size: " + filesize);

            }
            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String line) {
        try {
            printWriter.append(line + System.lineSeparator());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
