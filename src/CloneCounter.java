import java.io.*;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Farima on 8/1/2017.
 */
public class CloneCounter {
    public static void main(String[] args) {

        //    getCloneNumbersFromOneFile();
            //getCloneNumbersFromOneFile();
            //getClonesFromMultipleZippedFiles();
        //getClonesFromMultipleFiles();
//        getClonesFromMultipleZippedFiles_Typebased();
        getClonesFromMultipleFiles_Typebased();

    }

    private static void getCloneNumbersFromOneFile(){
        String path="/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/samples_baseline/Node4_5_6_equalCloneNonCloneSample.txt";
        int numclone=0;
        int numnonclone=0;
        try {
            BufferedReader bf = new BufferedReader(new FileReader(Paths.get(path).toString()));
            String line="";
            while ((line=bf.readLine())!=null){
                if (line.split("~~")[2].equals("1")) numclone++;
                else if (line.split("~~")[2].equals("0")) numnonclone++;
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("clone "+numclone);
        System.out.println("non clone "+numnonclone);
    }
    private static void getClonesFromMultipleFiles(){
        String pathFirstPart= "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
        String pathSecondPart= "6";
        int numclone=0;
        int numnonclone=0;
        String path=pathFirstPart+pathSecondPart;
        //path="D:\\farima";
        File folder = new File(path);
        for (File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                if (fileEntry.getName().endsWith("txt") && !fileEntry.getName().startsWith("candidatesList")) {

                    try {
                        BufferedReader bf = new BufferedReader(new FileReader(fileEntry));
                        String line = "";
                        while ((line = bf.readLine()) != null) {
                            if (line.split("~~")[2].equals("1")) numclone++;
                            else numnonclone++;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        System.out.println("clone "+numclone);
        System.out.println("non clone "+numnonclone);
    }

    private static void getClonesFromMultipleZippedFiles(){
        String zipDir="1";
        String pathToZip= "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/"+zipDir+".zip";
        //String pathToZip="D:\\farima.zip";
        int numclone=0;
        int numnonclone=0;
        ZipFile zip=null;
        try {
            zip = new ZipFile(pathToZip);
        }
        catch (IOException e){
         e.printStackTrace();
        }

        for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {

                ZipEntry entry = (ZipEntry) e.nextElement();
            try {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir+"/candidatesList")) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            if (line.split("~~")[2].equals("1")) numclone++;
                            else numnonclone++;
                        }
                    }
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
                System.out.println(entry.getName());
            }
        }


        System.out.println("clone "+numclone);
        System.out.println("non clone "+numnonclone);
    }

    private static void getClonesFromMultipleZippedFiles_Typebased(){
        String zipDir="2";
        String pathToZip= "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/"+zipDir+".zip";
        //String pathToZip="D:\\farima.zip";
        int type31_numclone=0,type32_numclone=0,type33_numclone=0;
        int type31_numNonclone=0,type32_numNonclone=0,type33_numNonclone=0;
        int numnonclone=0;
        ZipFile zip=null;
        try {
            zip = new ZipFile(pathToZip);
        }
        catch (IOException e){
            e.printStackTrace();
        }

        for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {

            ZipEntry entry = (ZipEntry) e.nextElement();
            try {
                if (!entry.isDirectory()) {
                    if (entry.getName().endsWith("txt") && !entry.getName().startsWith(zipDir+"/candidatesList")) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            String type=line.split(Pattern.quote("#$#"))[0];
                            if(type.equals("3.1")) {
                                if (line.split("~~")[2].equals("1")) type31_numclone++;
                                else type31_numNonclone++;
                            } else if(type.equals("3.2")) {
                                if (line.split("~~")[2].equals("1")) type32_numclone++;
                                else type32_numNonclone++;
                            } else if(type.equals("3.3")) {
                                if (line.split("~~")[2].equals("1")) type33_numclone++;
                                else type33_numNonclone++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex){
                ex.printStackTrace();
                System.out.println(entry.getName());
            }
        }


        System.out.println("type 3.1 clone "+type31_numclone);
        System.out.println("type 3.1 non clone "+type31_numNonclone);
        System.out.println("type 3.2 clone "+type32_numclone);
        System.out.println("type 3.2 non clone "+type32_numNonclone);
        System.out.println("type 3.3 clone "+type33_numclone);
        System.out.println("type 3.3 non clone "+type33_numNonclone);
    }

    private static void getClonesFromMultipleFiles_Typebased(){
        String pathFirstPart= "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
        String pathSecondPart= "6";
        //String pathToZip="D:\\farima.zip";
        int type31_numclone=0,type32_numclone=0,type33_numclone=0;
        int type31_numNonclone=0,type32_numNonclone=0,type33_numNonclone=0;
        String path=pathFirstPart+pathSecondPart;
        //path="D:\\farima";
        File folder = new File(path);
        for (File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                if (fileEntry.getName().endsWith("txt") && !fileEntry.getName().startsWith("candidatesList")) {
                    try {
                        BufferedReader bf = new BufferedReader(new FileReader(fileEntry));
                        String line;
                        while ((line = bf.readLine()) != null) {
                            String type=line.split(Pattern.quote("#$#"))[0];
                            if(type.equals("3.1")) {
                                if (line.split("~~")[2].equals("1")) type31_numclone++;
                                else type31_numNonclone++;
                            } else if(type.equals("3.2")) {
                                if (line.split("~~")[2].equals("1")) type32_numclone++;
                                else type32_numNonclone++;
                            } else if(type.equals("3.3")) {
                                if (line.split("~~")[2].equals("1")) type33_numclone++;
                                else type33_numNonclone++;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    }
                }
            }
        System.out.println("type 3.1 clone "+type31_numclone);
        System.out.println("type 3.1 non clone "+type31_numNonclone);
        System.out.println("type 3.2 clone "+type32_numclone);
        System.out.println("type 3.2 non clone "+type32_numNonclone);
        System.out.println("type 3.3 clone "+type33_numclone);
        System.out.println("type 3.3 non clone "+type33_numNonclone);
    }
}
