import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Farima on 7/12/2018.
 */
public class SampleEqualCloneNonclone_Typebased {

    private PrintWriter printWriter;

    public String type="3.1";

    SampleEqualCloneNonclone_Typebased() {
        try {

            printWriter=new PrintWriter("/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/samples_typebased/" +
                    "equal_"+type+".txt");
//            printWriter = new PrintWriter("D:\\equal.txt");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void main(String[] args) {
        SampleEqualCloneNonclone_Typebased sampler = new SampleEqualCloneNonclone_Typebased();
        sampler.sampleFromMultipleFilesFromZipNoCount_PerType();
    }

    public void sampleFromMultipleFilesFromZipNoCount_PerType(){
        try {
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
//            String mainPath = "D:\\farima\\";

            HashMap<Integer, String> idToFileLineNonclone = new HashMap<>();
            HashSet<String> clones = new HashSet<>();
            HashSet<String> selectedNonclones = new HashSet<>();
            String line = "";
            int numclone = 0;
            int countNonclone =0;
            File[] files = new File(mainPath).listFiles();
            BufferedReader bufferedReader = null;
            for (File file : files) {
                System.out.println("now reading "+file.getName());
                if (file.isDirectory()) {
                    File folder = new File(mainPath + file.getName());
                    for ( File entry : folder.listFiles()) {
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && entry.getName().startsWith(type) && !entry.getName().startsWith("candidatesList")) {
                                System.out.println(file.getName()+"    "+entry.getName());
                                bufferedReader = new BufferedReader(new FileReader(entry));
                                int lineNum = 0;
                                while ((line = bufferedReader.readLine()) != null) {
                                    try {
                                        if (line.split("~~")[2].equals("1")) {
                                            clones.add(entry.getName() + "_" + lineNum);
                                            numclone++;
                                        } else if (line.split("~~")[2].equals("0")) {
                                            idToFileLineNonclone.put(countNonclone++, entry.getName() + "_" + lineNum);
                                        }
                                        lineNum++;

                                    } catch (ArrayIndexOutOfBoundsException ex) {
                                        System.out.println("error in line: " + line);
                                        ex.printStackTrace();
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                } else if (!file.isDirectory() && file.getName().endsWith("zip")) {
		    
                    ZipFile zip = new ZipFile(mainPath + file.getName());
                    for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                        ZipEntry entry = (ZipEntry) e.nextElement();
                        if (!entry.isDirectory()) {
                            //System.out.println(entry.getName());
                            if (entry.getName().endsWith("txt") && entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip"))+"/"+type)
                                    && !entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip")) + "/candidatesList")) {
                                System.out.println(file.getName()+"    "+entry.getName());
                                bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                                int lineNum = 0;
                                while ((line = bufferedReader.readLine()) != null) {
                                    try {
                                        if (line.split("~~")[2].equals("1")) {
                                            clones.add(entry.getName() + "_" + lineNum);
                                            numclone++;
                                        } else if (line.split("~~")[2].equals("0")) {
                                            idToFileLineNonclone.put(countNonclone++, entry.getName() + "_" + lineNum);
                                        }
                                        lineNum++;

                                    } catch (ArrayIndexOutOfBoundsException ex) {
                                        System.out.println("error in line: " + line);
                                        ex.printStackTrace();
                                        continue;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println("num of clones "+numclone);
            System.out.println("num nonclones "+idToFileLineNonclone.size() );

            Random random = new Random(0);
            int randFileLine = 0;
            while (selectedNonclones.size() < numclone) {
                randFileLine = random.nextInt(idToFileLineNonclone.size());
                String lineFile = idToFileLineNonclone.get(randFileLine);
                if (!selectedNonclones.contains(lineFile)) {
                    selectedNonclones.add(lineFile);
                    //System.out.println(lineFile);
                }
            }

            System.out.println("random making complete");
            System.out.println(selectedNonclones.size());
            System.out.println("-----------------------------------------------------");
            int outputFilesize=0;
            files = new File(mainPath).listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    File folder = new File(mainPath + file.getName());
                    for ( File entry : folder.listFiles()) {
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt")&& entry.getName().startsWith(type) && !entry.getName().startsWith("candidatesList")) {
                                bufferedReader = new BufferedReader(new FileReader(entry));
                                line = "";
                                int lineNum = 0;
                                System.out.println("write "+file.getName()+"  "+entry.getName());
                                while ((line = bufferedReader.readLine()) != null) {
                                    if (selectedNonclones.contains(entry.getName() + "_" + lineNum) || clones.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesize++;
                                        writeToFile(line);
                                        //System.out.println(line);
                                    }
                                    lineNum++;
                                }
                            }
                        }
                    }
                } else if (!file.isDirectory() && file.getName().endsWith("zip")) {
                    ZipFile zip = new ZipFile(mainPath + file.getName());
                    for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                        ZipEntry entry = (ZipEntry) e.nextElement();
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip"))+"/"+type)
                                    && !entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip")) + "/candidatesList")) {
                                bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                                System.out.println("write "+file.getName()+"  "+entry.getName());
                                int lineNum = 0;
                                while ((line = bufferedReader.readLine()) != null) {
                                    if (selectedNonclones.contains(entry.getName() + "_" + lineNum) || clones.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesize++;
                                        writeToFile(line);
                                        //System.out.println(line);
                                    }
                                    lineNum++;
                                }
                            }
                        }
                    }
                }
            }

            printWriter.close();
            System.out.println("Write Complete. number of lines in file: " + outputFilesize);
            //printWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public void sampleFromMultipleFilesFromZipNoCount_Typebased(){
        try {
            String mainPath = "/lv_scratch/scratch/mondego/local/vaibhav/icse_2018/oreo/oreo/results/candidates/";
            //String mainPath = "D:\\farima\\";

            HashMap<Integer, String> idToFileLineType31 = new HashMap<>();
            HashMap<Integer, String> idToFileLineType32 = new HashMap<>();
            HashMap<Integer, String> idToFileLineType33 = new HashMap<>();
            HashSet<String> clonesType31 = new HashSet<>();
            HashSet<String> clonesType32 = new HashSet<>();
            HashSet<String> clonesType33 = new HashSet<>();
            HashSet<String> selectedNoncloneType31 = new HashSet<>();
            HashSet<String> selectedNoncloneType32 = new HashSet<>();
            HashSet<String> selectedNoncloneType33 = new HashSet<>();
            String line = "";
            int type31_numclone = 0, type32_numclone = 0, type33_numclone = 0;
            int countType31 = 0, countType32 = 0, countType33 = 0;
            File[] files = new File(mainPath).listFiles();
            BufferedReader bufferedReader = null;
            for (File file : files) {
                if (file.isDirectory()) {
                    File folder = new File(mainPath + file.getName());
                    for (final File entry : folder.listFiles()) {
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                                bufferedReader = new BufferedReader(new FileReader(entry));
                                int lineNum = 0;
                                while ((line = bufferedReader.readLine()) != null) {
                                    try {
                                        String type = line.split(Pattern.quote("#$#"))[0];
                                        if (line.split("~~")[2].equals("1")) {
                                            if (type.equals("3.1")) {
                                                clonesType31.add(entry.getName() + "_" + lineNum);
                                                //System.out.println(entry.getName()+"_"+lineNum);
                                                type31_numclone++;
                                            } else if (type.equals("3.2")) {
                                                clonesType32.add(entry.getName() + "_" + lineNum);
//                                        System.out.println(entry.getName()+"_"+lineNum);
                                                type32_numclone++;
                                            } else if (type.equals("3.3")) {
                                                clonesType33.add(entry.getName() + "_" + lineNum);
//                                        System.out.println(entry.getName()+"_"+lineNum);
                                                type33_numclone++;
                                            }
                                        }


                                        if (type.equals("3.1")) {
                                            idToFileLineType31.put(countType31++, entry.getName() + "_" + lineNum);
                                        } else if (type.equals("3.2")) {
                                            idToFileLineType32.put(countType32++, entry.getName() + "_" + lineNum);
                                        } else if (type.equals("3.3")) {
                                            idToFileLineType33.put(countType33++, entry.getName() + "_" + lineNum);
                                        }
                                        lineNum++;

                                    } catch (ArrayIndexOutOfBoundsException ex) {
                                        System.out.println("error in line: " + line);
                                        ex.printStackTrace();
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                } else if (!file.isDirectory() && file.getName().endsWith("zip")) {
                    ZipFile zip = new ZipFile(mainPath + file.getName());
                    for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                        ZipEntry entry = (ZipEntry) e.nextElement();
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && !entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip")) + "/candidatesList")) {
                                bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                                int lineNum = 0;
                                while ((line = bufferedReader.readLine()) != null) {
                                    try {
                                        String type = line.split(Pattern.quote("#$#"))[0];
                                        if (line.split("~~")[2].equals("1")) {
                                            if (type.equals("3.1")) {
                                                clonesType31.add(entry.getName() + "_" + lineNum);
                                                //System.out.println(entry.getName()+"_"+lineNum);
                                                type31_numclone++;
                                            } else if (type.equals("3.2")) {
                                                clonesType32.add(entry.getName() + "_" + lineNum);
//                                        System.out.println(entry.getName()+"_"+lineNum);
                                                type32_numclone++;
                                            } else if (type.equals("3.3")) {
                                                clonesType33.add(entry.getName() + "_" + lineNum);
//                                        System.out.println(entry.getName()+"_"+lineNum);
                                                type33_numclone++;
                                            }
                                        }


                                        if (type.equals("3.1")) {
                                            idToFileLineType31.put(countType31++, entry.getName() + "_" + lineNum);
                                        } else if (type.equals("3.2")) {
                                            idToFileLineType32.put(countType32++, entry.getName() + "_" + lineNum);
                                        } else if (type.equals("3.3")) {
                                            idToFileLineType33.put(countType33++, entry.getName() + "_" + lineNum);
                                        }
                                        lineNum++;

                                    } catch (ArrayIndexOutOfBoundsException ex) {
                                        System.out.println("error in line: " + line);
                                        ex.printStackTrace();
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            System.out.println("reading complete, now random making starts....");
            System.out.println(type31_numclone);
            System.out.println(type32_numclone);
            System.out.println(type33_numclone);

            Random random = new Random(0);
            int randFileLine = 0;
            while (selectedNoncloneType31.size() < type31_numclone) {
                randFileLine = random.nextInt(idToFileLineType31.size());
                String lineFile = idToFileLineType31.get(randFileLine);
                if (!selectedNoncloneType31.contains(lineFile) && !clonesType31.contains(lineFile)) {
                    selectedNoncloneType31.add(lineFile);
                    System.out.println(lineFile);
                }
            }
            while (selectedNoncloneType32.size() < type32_numclone) {
                randFileLine = random.nextInt(idToFileLineType32.size());
                String lineFile = idToFileLineType32.get(randFileLine);
                if (!selectedNoncloneType32.contains(lineFile) && !clonesType32.contains(lineFile)) {
                    selectedNoncloneType32.add(lineFile);
                    System.out.println(lineFile);
                }
            }
            while (selectedNoncloneType33.size() < type33_numclone) {
                randFileLine = random.nextInt(idToFileLineType33.size());
                String lineFile = idToFileLineType33.get(randFileLine);
                if (!selectedNoncloneType33.contains(lineFile) && !clonesType33.contains(lineFile)) {
                    selectedNoncloneType33.add(lineFile);
                    System.out.println(lineFile);
                }
            }
            System.out.println("random making complete");
            System.out.println(selectedNoncloneType31.size());
            System.out.println(selectedNoncloneType32.size());
            System.out.println(selectedNoncloneType33.size());
            System.out.println("-----------------------------------------------------");
            int outputFilesizeType31 = 0, outputFilesizeType32 = 0, outputFilesizeType33 = 0;

            for (File file : files) {
                if (file.isDirectory()) {
                    File folder = new File(mainPath + file.getName());
                    for (final File entry : folder.listFiles()) {
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && !entry.getName().startsWith("candidatesList")) {
                                bufferedReader = new BufferedReader(new FileReader(entry));
                                line = "";
                                int lineNum = 0;
                                //System.out.println(lineNum);
                                while ((line = bufferedReader.readLine()) != null) {
                                    if (selectedNoncloneType31.contains(entry.getName() + "_" + lineNum) || clonesType31.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType31++;
                                        writeToFile_Type31(line);
                                        if (selectedNoncloneType31.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    if (selectedNoncloneType32.contains(entry.getName() + "_" + lineNum) || clonesType32.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType32++;
                                        writeToFile_Type32(line);
                                        if (selectedNoncloneType32.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    if (selectedNoncloneType33.contains(entry.getName() + "_" + lineNum) || clonesType33.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType33++;
                                        writeToFile_Type33(line);
                                        if (selectedNoncloneType33.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    lineNum++;

                                }
                            }
                        }
                    }
                } else if (!file.isDirectory() && file.getName().endsWith("zip")) {
                    ZipFile zip = new ZipFile(mainPath + file.getName());
                    for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
                        ZipEntry entry = (ZipEntry) e.nextElement();
                        if (!entry.isDirectory()) {
                            if (entry.getName().endsWith("txt") && !entry.getName().startsWith(file.getName().substring(0, file.getName().indexOf(".zip")) + "/candidatesList")) {
                                bufferedReader = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)));
                                line = "";
                                int lineNum = 0;
                                //System.out.println(lineNum);
                                while ((line = bufferedReader.readLine()) != null) {
                                    if (selectedNoncloneType31.contains(entry.getName() + "_" + lineNum) || clonesType31.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType31++;
                                        writeToFile_Type31(line);
                                        if (selectedNoncloneType31.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    if (selectedNoncloneType32.contains(entry.getName() + "_" + lineNum) || clonesType32.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType32++;
                                        writeToFile_Type32(line);
                                        if (selectedNoncloneType32.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    if (selectedNoncloneType33.contains(entry.getName() + "_" + lineNum) || clonesType33.contains(entry.getName() + "_" + lineNum)) {
                                        //System.out.println(entry.getName()+"_"+lineNum);
                                        outputFilesizeType33++;
                                        writeToFile_Type33(line);
                                        if (selectedNoncloneType33.contains(entry.getName() + "_" + lineNum))
                                            System.out.println(line);
                                    }
                                    lineNum++;

                                }
                            }
                        }
                    }
                }
            }


            System.out.println("Write Complete. number of lines in type 3.1 file: " + outputFilesizeType31);
            System.out.println("Write Complete. number of lines in type 3.2 file: " + outputFilesizeType32);
            System.out.println("Write Complete. number of lines in type 3.3 file: " + outputFilesizeType33);
            printWriterType31.close();
            printWriterType32.close();
            printWriterType33.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    private void writeToFile(String line) {
        try {
            printWriter.append(line + System.lineSeparator());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
